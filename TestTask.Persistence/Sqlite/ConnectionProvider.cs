﻿using Microsoft.Data.Sqlite;
using TestTask.Persistence.Interfaces.Sqlite;

namespace TestTask.Persistence.Sqlite
{
    public class ConnectionProvider : IConnectionProvider
    {
        private readonly string _connectionString;

        public ConnectionProvider(string connectionString)
        {
            _connectionString = connectionString;
        }

        public SqliteConnection GetConnection()
        {
            return new SqliteConnection(_connectionString);
        }
    }
}