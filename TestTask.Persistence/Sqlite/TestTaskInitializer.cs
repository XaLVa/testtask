﻿using TestTask.Persistence.Interfaces.Sqlite;

namespace TestTask.Persistence.Sqlite
{
    public class TestTaskInitializer : IInitializer
    {
        private IConnectionProvider _connectionProvider;

        public TestTaskInitializer(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public void Init()
        {
            DropAll();
            CreateFlight();
            CreateAccommodation();
            CreateExcursion();
            CreateTour();
            CreateToursExcursions();
        }

        private void DropAll()
        {
            using var con = _connectionProvider.GetConnection();

            con.Open();

            var createFlightCom = con.CreateCommand();
            createFlightCom.CommandText = "DROP TABLE IF EXISTS tours_excursions; " +
                                          "DROP TABLE IF EXISTS tour; " +
                                          "DROP TABLE IF EXISTS flight; " +
                                          "DROP TABLE IF EXISTS accommodation; " +
                                          "DROP TABLE IF EXISTS excursion;";
            createFlightCom.ExecuteNonQuery();
        }

        private void CreateTour()
        {
            using var con = _connectionProvider.GetConnection();

            con.Open();

            var createFlightCom = con.CreateCommand();
            createFlightCom.CommandText = "CREATE TABLE tour( " +
                                          "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                          "flight_id INTEGER, " +
                                          "accommodation_id INTEGER, " +
                                          "organizer TEXT, " +
                                          "FOREIGN KEY(flight_id) REFERENCES flight(id), " +
                                          "FOREIGN KEY(accommodation_id) REFERENCES accommodation(id));";
            createFlightCom.ExecuteNonQuery();
        }

        private void CreateFlight()
        {
            using var con = _connectionProvider.GetConnection();
            
            con.Open();

            var createFlightCom = con.CreateCommand();
            createFlightCom.CommandText = "CREATE TABLE flight( " +
                                          "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                          "seat_count INTEGER, " +
                                          "price REAL);";
            createFlightCom.ExecuteNonQuery();
        }

        private void CreateAccommodation()
        {
            using var con = _connectionProvider.GetConnection();

            con.Open();

            var createFlightCom = con.CreateCommand();
            createFlightCom.CommandText = "CREATE TABLE accommodation( " +
                                          "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                          "room_count INTEGER, " +
                                          "price REAL);";
            createFlightCom.ExecuteNonQuery();
        }

        private void CreateExcursion()
        {
            using var con = _connectionProvider.GetConnection();

            con.Open();

            var createFlightCom = con.CreateCommand();
            createFlightCom.CommandText = "CREATE TABLE excursion( " +
                                          "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                          "title TEXT, " +
                                          "visitor_count INTEGER, " +
                                          "price REAL);";
            createFlightCom.ExecuteNonQuery();
        }

        private void CreateToursExcursions()
        {
            using var con = _connectionProvider.GetConnection();

            con.Open();

            var createFlightCom = con.CreateCommand();
            createFlightCom.CommandText = "CREATE TABLE tours_excursions( " +
                                          "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                          "tour_id INTEGER, " +
                                          "excursion_id INTEGER, " +
                                          "FOREIGN KEY(tour_id) REFERENCES tour(id), " +
                                          "FOREIGN KEY(excursion_id) REFERENCES excursion(id));";
            createFlightCom.ExecuteNonQuery();
        }
    }
}