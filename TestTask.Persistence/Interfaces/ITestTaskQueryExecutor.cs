﻿using System.Collections.Generic;

namespace TestTask.Persistence.Interfaces
{
    public interface ITestTaskQueryExecutor
    {
        IEnumerable<object> GetToursWithExcursions();

        IEnumerable<object> GetTourMaxExcursionPrices();

        IEnumerable<object> GetTourTotalPrices();
    }
}