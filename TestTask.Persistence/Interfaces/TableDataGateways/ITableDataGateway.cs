﻿using System.Collections.Generic;
using TestTask.Persistence.Models;

namespace TestTask.Persistence.Interfaces.TableDataGateways
{
    public interface ITableDataGateway<T> where T : Entity
    {
        public IEnumerable<T> FindAll();

        public T FindById(int id);

        public void Insert(T item);

        public void Update(int id, T item);

        public void Delete(int id);
    }
}