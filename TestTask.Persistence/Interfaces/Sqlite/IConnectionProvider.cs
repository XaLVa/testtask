﻿using Microsoft.Data.Sqlite;

namespace TestTask.Persistence.Interfaces.Sqlite
{
    public interface IConnectionProvider
    {
        SqliteConnection GetConnection();
    }
}