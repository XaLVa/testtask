﻿namespace TestTask.Persistence.Interfaces.Sqlite
{
    public interface IInitializer
    {
        void Init();
    }
}