﻿using System.Collections.Generic;
using System.Data;
using TestTask.Persistence.Extensions.Sqlite;
using TestTask.Persistence.Interfaces;
using TestTask.Persistence.Interfaces.Sqlite;

namespace TestTask.Persistence
{
    public class TestTaskQueryExecutor : ITestTaskQueryExecutor
    {
        private IConnectionProvider _connectionProvider;

        public TestTaskQueryExecutor(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public IEnumerable<object> GetToursWithExcursions()
        {
            using var connection = _connectionProvider.GetConnection();
            connection.Open();

            var command = connection.CreateCommand();
            command.CommandText = "SELECT tour.id, title FROM tour " +
                                  "LEFT JOIN tours_excursions ON tour_id = tour.id " +
                                  "LEFT JOIN excursion ON excursion_id = excursion.id;";

            var objects = new List<object>();

            var reader = command.ExecuteReader();

            if (!reader.HasRows) return objects;

            while (reader.Read())
            {
                objects.Add(new
                {
                    TourId = reader.GetInt32(0),
                    ExcursionTitle = reader.GetStringNullSafe(1)
                });
            }

            return objects;
        }

        public IEnumerable<object> GetTourMaxExcursionPrices()
        {
            using var connection = _connectionProvider.GetConnection();
            connection.Open();

            var command = connection.CreateCommand();
            command.CommandText = "SELECT tour.id, max(price) AS max_price FROM tour " +
                                  "JOIN tours_excursions ON tour_id = tour.id " +
                                  "JOIN excursion ON excursion_id = excursion.id " +
                                  "GROUP BY tour.id;";

            var objects = new List<object>();

            var reader = command.ExecuteReader();

            if (!reader.HasRows) return objects;

            while (reader.Read())
            {
                objects.Add(new
                {
                    TourId = reader.GetInt32(0),
                    MaxPrice = reader.GetDouble(1)
                });
            }

            return objects;
        }

        public IEnumerable<object> GetTourTotalPrices()
        {
            using var connection = _connectionProvider.GetConnection();
            connection.Open();

            var command = connection.CreateCommand();
            command.CommandText = "WITH cte (tour_id, total_price) AS (" +
                                  "SELECT tour.id, SUM(seat_count * price) FROM tour " +
                                  "JOIN flight ON flight_id = flight.id " +
                                  "GROUP BY tour.id " +
                                  "UNION ALL " +
                                  "SELECT tour.id, SUM(room_count * price) FROM tour " +
                                  "JOIN accommodation ON accommodation_id = accommodation.id " +
                                  "GROUP BY tour.id " +
                                  "UNION ALL " +
                                  "SELECT tour.id, SUM(visitor_count * price) FROM tour " +
                                  "JOIN tours_excursions te ON tour_id = tour.id " +
                                  "JOIN excursion ON excursion_id = excursion.id " +
                                  "GROUP BY tour.id) " +
                                  "SELECT tour_id, SUM(total_price) FROM cte " +
                                  "GROUP BY tour_id;";

            var objects = new List<object>();

            var reader = command.ExecuteReader();

            if (!reader.HasRows) return objects;

            while (reader.Read())
            {
                objects.Add(new
                {
                    TourId = reader.GetInt32(0),
                    TotalPrice = reader.GetDouble(1)
                });
            }

            return objects;
        }
    }
}