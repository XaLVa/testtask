﻿using Microsoft.Data.Sqlite;
using TestTask.Persistence.Models;

namespace TestTask.Persistence.Utils
{
    public class ModelParser
    {
        public static Tour ParseTour(SqliteDataReader reader)
        {

            return 
                new Tour()
                {
                    Id = reader.GetInt32(0),
                    FlightId = reader.GetInt32(1),
                    AccommodationId = reader.GetInt32(2),
                    Organizer = reader.GetString(3)
                };
        }

        public static Flight ParseFlight(SqliteDataReader reader)
        {
            return
                new Flight()
                {
                    Id = reader.GetInt32(0),
                    SeatCount = reader.GetInt32(1),
                    Price = reader.GetDouble(2)
                };
            }

        public static Accommodation ParseAccommodation(SqliteDataReader reader)
        {
            return
                new Accommodation()
                {
                    Id = reader.GetInt32(0),
                    RoomCount = reader.GetInt32(1),
                    Price = reader.GetDouble(2)
                };
        }

        public static Excursion ParseExcursion(SqliteDataReader reader)
        {
            return
                new Excursion()
                {
                    Id = reader.GetInt32(0),
                    Title = reader.GetString(1),
                    VisitorCount = reader.GetInt32(2),
                    Price = reader.GetDouble(3)
                };
        }

        public static ToursExcursions ParseToursExcursions(SqliteDataReader reader)
        {
            return 
                new ToursExcursions()
                {
                    Id = reader.GetInt32(0),
                    TourId = reader.GetInt32(1),
                    ExcursionId = reader.GetInt32(2)
                };
        }
    }
}