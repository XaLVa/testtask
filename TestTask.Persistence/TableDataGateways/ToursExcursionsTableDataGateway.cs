﻿using System.Collections.Generic;
using TestTask.Persistence.Interfaces.Sqlite;
using TestTask.Persistence.Interfaces.TableDataGateways;
using TestTask.Persistence.Models;
using TestTask.Persistence.Utils;

namespace TestTask.Persistence.TableDataGateways
{
    public class ToursExcursionsTableDataGateway : ITableDataGateway<ToursExcursions>
    {
        private IConnectionProvider _connectionProvider;

        public ToursExcursionsTableDataGateway(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public IEnumerable<ToursExcursions> FindAll()
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "SELECT id, tour_id, excursion_id FROM tours_excursions;";

            var reader = command.ExecuteReader();

            var toursExcursions = new List<ToursExcursions>();

            if (!reader.HasRows) return toursExcursions;

            while (reader.Read())
            {
                toursExcursions.Add(ModelParser.ParseToursExcursions(reader));
            }

            return toursExcursions;
        }

        public ToursExcursions FindById(int id)
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "SELECT id, tour_id, excursion_id FROM tours_excursions " +
                                  "WHERE id = @id;";

            command.Parameters.AddWithValue("@id", id);

            var reader = command.ExecuteReader();

            if (!reader.HasRows) return null;

            reader.Read();

            return ModelParser.ParseToursExcursions(reader);
        }

        public void Insert(ToursExcursions item)
        {
            if (item == null) return;

            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "INSERT INTO tours_excursions(tour_id, excursion_id) " +
                                  "VALUES(@tour_id, @excursion_id);";

            command.Parameters.AddWithValue("@tour_id", item.TourId);
            command.Parameters.AddWithValue("@excursion_id", item.ExcursionId);

            command.ExecuteNonQuery();
        }

        public void Update(int id, ToursExcursions item)
        {
            if (item == null) return;

            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "UPDATE tours_excursions " +
                                  "SET tour_id = @tour_id, " +
                                  "excursion_id = @excursion_id " +
                                  "WHERE id = @id;";

            command.Parameters.AddWithValue("@tour_id", item.TourId);
            command.Parameters.AddWithValue("@excursion_id", item.ExcursionId);
            command.Parameters.AddWithValue("@id", id);

            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "DELETE FROM tours_excursions WHERE id = @id;";

            command.Parameters.AddWithValue("@id", id);

            command.ExecuteNonQuery();
        }
    }
}