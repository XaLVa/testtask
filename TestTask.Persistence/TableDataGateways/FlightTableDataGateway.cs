﻿using System;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;
using TestTask.Persistence.Interfaces.Sqlite;
using TestTask.Persistence.Interfaces.TableDataGateways;
using TestTask.Persistence.Models;
using TestTask.Persistence.Utils;

namespace TestTask.Persistence.TableDataGateways
{
    public class FlightTableDataGateway : ITableDataGateway<Flight>
    {
        private IConnectionProvider _connectionProvider;

        public FlightTableDataGateway(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public IEnumerable<Flight> FindAll()
        {
            using var connection = _connectionProvider.GetConnection();
            connection.Open();

            var command = connection.CreateCommand();
            command.CommandText = "SELECT id, seat_count, price FROM flight;";

            var flights = new List<Flight>();

            var reader = command.ExecuteReader();

            if (!reader.HasRows) return flights;

            while (reader.Read())
            {
                flights.Add(ModelParser.ParseFlight(reader));
            }

            return flights;
        }

        public Flight FindById(int id)
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "SELECT id, seat_count, price FROM flight " +
                                  "WHERE id = @id;";

            command.Parameters.AddWithValue("@id", id);

            var reader = command.ExecuteReader();

            if (!reader.HasRows) return null;

            reader.Read();

            return ModelParser.ParseFlight(reader);
        }

        public void Insert(Flight item)
        {
            if (item == null) return;

            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "INSERT INTO flight(seat_count, price) " +
                                  "VALUES(@seat_count, @price);";

            command.Parameters.AddWithValue("@seat_count", item.SeatCount);
            command.Parameters.AddWithValue("@price", item.Price);

            command.ExecuteNonQuery();
        }

        public void Update(int id, Flight item)
        {
            if (item == null) return;

            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "UPDATE flight " +
                                  "SET seat_count = @seat_count, " +
                                  "price = @price " +
                                  "WHERE id = @id;";

            command.Parameters.AddWithValue("@seat_count", item.SeatCount);
            command.Parameters.AddWithValue("@price", item.Price);
            command.Parameters.AddWithValue("@id", id);

            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "DELETE FROM flight WHERE id = @id;";

            command.Parameters.AddWithValue("@id", id);

            command.ExecuteNonQuery();
        }
    }
}