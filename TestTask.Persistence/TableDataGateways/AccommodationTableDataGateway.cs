﻿using System.Collections.Generic;
using TestTask.Persistence.Interfaces.Sqlite;
using TestTask.Persistence.Interfaces.TableDataGateways;
using TestTask.Persistence.Models;
using TestTask.Persistence.Utils;

namespace TestTask.Persistence.TableDataGateways
{
    public class AccommodationTableDataGateway : ITableDataGateway<Accommodation>
    {
        private IConnectionProvider _connectionProvider;

        public AccommodationTableDataGateway(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public IEnumerable<Accommodation> FindAll()
        {
            using var connection = _connectionProvider.GetConnection();
            connection.Open();

            var command = connection.CreateCommand();
            command.CommandText = "SELECT id, room_count, price FROM accommodation;";

            var accommodations = new List<Accommodation>();

            var reader = command.ExecuteReader();

            if (!reader.HasRows) return accommodations;

            while (reader.Read())
            {
                accommodations.Add(ModelParser.ParseAccommodation(reader));
            }

            return accommodations;
        }

        public Accommodation FindById(int id)
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "SELECT id, room_count, price FROM accommodation " +
                                  "WHERE id = @id;";

            command.Parameters.AddWithValue("@id", id);

            var reader = command.ExecuteReader();

            if (!reader.HasRows) return null;

            reader.Read();

            return ModelParser.ParseAccommodation(reader);
        }

        public void Insert(Accommodation item)
        {
            if (item == null) return;

            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "INSERT INTO accommodation(room_count, price) " +
                                  "VALUES(@room_count, @price);";

            command.Parameters.AddWithValue("@room_count", item.RoomCount);
            command.Parameters.AddWithValue("@price", item.Price);

            command.ExecuteNonQuery();
        }

        public void Update(int id, Accommodation item)
        {
            if (item == null) return;

            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "UPDATE accommodation " +
                                  "SET room_count = @room_count, " +
                                  "price = @price " +
                                  "WHERE id = @id;";

            command.Parameters.AddWithValue("@room_count", item.RoomCount);
            command.Parameters.AddWithValue("@price", item.Price);
            command.Parameters.AddWithValue("@id", id);

            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "DELETE FROM accommodation WHERE id = @id;";

            command.Parameters.AddWithValue("@id", id);

            command.ExecuteNonQuery();
        }
    }
}