﻿using System.Collections.Generic;
using TestTask.Persistence.Interfaces.Sqlite;
using TestTask.Persistence.Interfaces.TableDataGateways;
using TestTask.Persistence.Models;
using TestTask.Persistence.Utils;

namespace TestTask.Persistence.TableDataGateways
{
    public class ExcursionTableDataGateway : ITableDataGateway<Excursion>
    {
        private IConnectionProvider _connectionProvider;

        public ExcursionTableDataGateway(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public IEnumerable<Excursion> FindAll()
        {
            using var connection = _connectionProvider.GetConnection();
            connection.Open();

            var command = connection.CreateCommand();
            command.CommandText = "SELECT id, title, visitor_count, price FROM excursion;";

            var excursions = new List<Excursion>();

            var reader = command.ExecuteReader();

            if (!reader.HasRows) return excursions;

            while (reader.Read())
            {
                excursions.Add(ModelParser.ParseExcursion(reader));
            }

            return excursions;
        }

        public Excursion FindById(int id)
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "SELECT id, title, visitor_count, price FROM excursion " +
                                  "WHERE id = @id;";

            command.Parameters.AddWithValue("@id", id);

            var reader = command.ExecuteReader();

            if (!reader.HasRows) return null;

            reader.Read();

            return ModelParser.ParseExcursion(reader);
        }

        public void Insert(Excursion item)
        {
            if (item == null) return;

            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "INSERT INTO excursion(title, visitor_count, price) " +
                                  "VALUES(@title, @visitor_count, @price);";

            command.Parameters.AddWithValue("@title", item.Title);
            command.Parameters.AddWithValue("@visitor_count", item.VisitorCount);
            command.Parameters.AddWithValue("@price", item.Price);

            command.ExecuteNonQuery();
        }

        public void Update(int id, Excursion item)
        {
            if (item == null) return;

            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "UPDATE excursion " +
                                  "SET title = @title, " +
                                  "visitor_count = @visitor_count " +
                                  "price = @price " +
                                  "WHERE id = @id;";

            command.Parameters.AddWithValue("@title", item.Title);
            command.Parameters.AddWithValue("@visitor_count", item.VisitorCount);
            command.Parameters.AddWithValue("@price", item.Price);
            command.Parameters.AddWithValue("@id", id);

            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "DELETE FROM excursion WHERE id = @id;";

            command.Parameters.AddWithValue("@id", id);

            command.ExecuteNonQuery();
        }
    }
}