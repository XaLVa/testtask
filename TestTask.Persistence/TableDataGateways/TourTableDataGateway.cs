﻿using System.Collections.Generic;
using System.Data;
using TestTask.Persistence.Interfaces.Sqlite;
using TestTask.Persistence.Interfaces.TableDataGateways;
using TestTask.Persistence.Models;
using TestTask.Persistence.Utils;

namespace TestTask.Persistence.TableDataGateways
{
    public class TourTableDataGateway : ITableDataGateway<Tour>
    {
        private IConnectionProvider _connectionProvider;

        public TourTableDataGateway(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public IEnumerable<Tour> FindAll()
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "SELECT id, flight_id, accommodation_id, organizer FROM tour;";

            var reader = command.ExecuteReader();

            var tours = new List<Tour>();

            if (!reader.HasRows) return tours;

            while (reader.Read())
            {
                tours.Add(ModelParser.ParseTour(reader));
            }

            return tours;
        }

        public Tour FindById(int id)
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "SELECT id, flight_id, accommodation_id, organizer FROM tour " +
                                  "WHERE id = @id;";

            command.Parameters.AddWithValue("@id", id);

            var reader = command.ExecuteReader();

            if (!reader.HasRows) return null;

            reader.Read();

            return ModelParser.ParseTour(reader);
        }

        public void Insert(Tour item)
        {
            if (item == null) return;

            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "INSERT INTO tour(flight_id, accommodation_id, organizer) " +
                                  "VALUES(@flight_id, @accommodation_id, @organizer);";

            command.Parameters.AddWithValue("@flight_id", item.FlightId);
            command.Parameters.AddWithValue("@accommodation_id", item.AccommodationId);
            command.Parameters.AddWithValue("@organizer", item.Organizer);

            command.ExecuteNonQuery();
        }

        public void Update(int id, Tour item)
        {
            if (item == null) return;

            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "UPDATE tour " +
                                  "SET flight_id = @flight_id, " +
                                  "accommodation_id = @accommodation_id, " +
                                  "organizer = @organizer " +
                                  "WHERE id = @id;";

            command.Parameters.AddWithValue("@flight_id", item.FlightId);
            command.Parameters.AddWithValue("@accommodation_id", item.AccommodationId);
            command.Parameters.AddWithValue("@organizer", item.Organizer);
            command.Parameters.AddWithValue("@id", id);

            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            using var connection = _connectionProvider.GetConnection();

            connection.Open();

            var command = connection.CreateCommand();

            command.CommandText = "DELETE FROM tour WHERE id = @id;";

            command.Parameters.AddWithValue("@id", id);

            command.ExecuteNonQuery();
        }
    }
}