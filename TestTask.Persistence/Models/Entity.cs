﻿using System.Text;
using TestTask.Persistence.Utils;

namespace TestTask.Persistence.Models
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}