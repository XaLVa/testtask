﻿namespace TestTask.Persistence.Models
{
    public class ToursExcursions : Entity
    {
        public int TourId { get; set; }

        public int ExcursionId { get; set; }
    }
}