﻿using System.Text;
using TestTask.Persistence.Utils;

namespace TestTask.Persistence.Models
{
    public class Flight : Entity
    {
        public int SeatCount { get; set; }

        public double Price { get; set; }
    }
}