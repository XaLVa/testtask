﻿namespace TestTask.Persistence.Models
{
    public class Tour : Entity
    {
        public int FlightId { get; set; }

        public int AccommodationId { get; set; }

        public string Organizer { get; set; }
    }
}