﻿namespace TestTask.Persistence.Models
{
    public class Excursion : Entity
    {
        public string Title { get; set; }

        public int VisitorCount { get; set; }

        public double Price { get; set; }
    }
}