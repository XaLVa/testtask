﻿namespace TestTask.Persistence.Models
{
    public class Accommodation : Entity
    {
        public int RoomCount { get; set; }

        public double Price { get; set; }
    }
}