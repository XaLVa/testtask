﻿using Microsoft.Data.Sqlite;

namespace TestTask.Persistence.Extensions.Sqlite
{
    public static class DateReaderExtension
    {
        public static string GetStringNullSafe(this SqliteDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal)) return "NULL";

            return reader.GetString(ordinal);
        }
    }
}