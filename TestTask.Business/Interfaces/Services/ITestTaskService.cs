﻿using System.Collections.Generic;
using TestTask.Persistence.Models;

namespace TestTask.Business.Interfaces.Services
{
    public interface ITestTaskService
    {
        void Init();

        IEnumerable<object> GetToursWithExcursions();

        IEnumerable<object> GetTourMaxExcursionPrices();

        IEnumerable<object> GetTourTotalPrices();
    }
}