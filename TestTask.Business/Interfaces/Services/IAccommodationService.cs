﻿using TestTask.Persistence.Models;

namespace TestTask.Business.Interfaces.Services
{
    public interface IAccommodationService : ICrudService<Accommodation>
    {
        
    }
}