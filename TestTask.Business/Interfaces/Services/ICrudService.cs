﻿using System.Collections.Generic;
using TestTask.Persistence.Models;

namespace TestTask.Business.Interfaces.Services
{
    public interface ICrudService<T> where T : Entity
    {
        IEnumerable<T> GetAll();

        T GetById(int id);

        void Create(T item);

        void Update(int id, T item);

        void Delete(int id);
    }
}