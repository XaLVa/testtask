﻿using System.Collections.Generic;
using TestTask.Business.Interfaces.Services;
using TestTask.Persistence.Interfaces.TableDataGateways;
using TestTask.Persistence.Models;

namespace TestTask.Business.Services
{
    public class FlightService : IFlightService
    {
        private ITableDataGateway<Flight> _flightGateway;

        public FlightService(ITableDataGateway<Flight> flightGateway)
        {
            _flightGateway = flightGateway;
        }

        public IEnumerable<Flight> GetAll()
        {
            return _flightGateway.FindAll();
        }

        public Flight GetById(int id)
        {
            return _flightGateway.FindById(id);
        }

        public void Create(Flight item)
        {
            _flightGateway.Insert(item);
        }

        public void Update(int id, Flight item)
        {
            _flightGateway.Update(id, item);
        }

        public void Delete(int id)
        {
            _flightGateway.Delete(id);
        }
    }
}