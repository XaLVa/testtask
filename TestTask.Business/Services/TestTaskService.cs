﻿using System.Collections.Generic;
using TestTask.Business.Interfaces.Services;
using TestTask.Persistence;
using TestTask.Persistence.Interfaces;
using TestTask.Persistence.Interfaces.Sqlite;
using TestTask.Persistence.Models;

namespace TestTask.Business.Services
{
    public class TestTaskService : ITestTaskService
    {
        private IInitializer _initializer;

        private ITestTaskQueryExecutor _testTaskQueryExecutor;

        private ITourService _tourService;
        private IFlightService _flightService;
        private IAccommodationService _accommodationService;
        private IExcursionService _excursionService;
        private IToursExcursionsService _toursExcursionsService;

        public TestTaskService(IInitializer initializer, ITourService tourService, IFlightService flightService, IAccommodationService accommodationService, IExcursionService excursionService, IToursExcursionsService toursExcursionsService, ITestTaskQueryExecutor testTaskQueryExecutor)
        {
            _initializer = initializer;
            _tourService = tourService;
            _flightService = flightService;
            _accommodationService = accommodationService;
            _excursionService = excursionService;
            _toursExcursionsService = toursExcursionsService;
            _testTaskQueryExecutor = testTaskQueryExecutor;
        }

        public void Init()
        {
            _initializer.Init();
            FillExampleData();
        }

        public IEnumerable<object> GetToursWithExcursions()
        {
            return _testTaskQueryExecutor.GetToursWithExcursions();
        }

        public IEnumerable<object> GetTourMaxExcursionPrices()
        {
            return _testTaskQueryExecutor.GetTourMaxExcursionPrices();
        }

        public IEnumerable<object> GetTourTotalPrices()
        {
            return _testTaskQueryExecutor.GetTourTotalPrices();
        }

        private void FillExampleData()
        {
            _flightService.Create(new Flight()
            {
                SeatCount = 1,
                Price = 10
            });

            _flightService.Create(new Flight()
            {
                SeatCount = 2,
                Price = 12
            });

            _flightService.Create(new Flight()
            {
                SeatCount = 1,
                Price = 20
            });

            _accommodationService.Create(new Accommodation()
            {
                RoomCount = 1,
                Price = 100
            });

            _accommodationService.Create(new Accommodation()
            {
                RoomCount = 2,
                Price = 90
            });

            _accommodationService.Create(new Accommodation()
            {
                RoomCount = 1,
                Price = 120
            });

            _excursionService.Create(new Excursion()
            {
                Title = "City overview",
                VisitorCount = 2,
                Price = 35
            });

            _excursionService.Create(new Excursion()
            {
                Title = "City overview",
                VisitorCount = 1,
                Price = 35
            });

            _excursionService.Create(new Excursion()
            {
                Title = "Museum",
                VisitorCount = 1,
                Price = 50
            });

            _tourService.Create(new Tour()
            {
                FlightId = 1,
                AccommodationId = 1,
                Organizer = "TravelEverywhere"
            });

            _tourService.Create(new Tour()
            {
                FlightId = 2,
                AccommodationId = 2,
                Organizer = "TravelEverywhere"
            });

            _tourService.Create(new Tour()
            {
                FlightId = 3,
                AccommodationId = 3,
                Organizer = "TravelRich"
            });

            _toursExcursionsService.Create(new ToursExcursions()
            {
                TourId = 2,
                ExcursionId = 1
            });

            _toursExcursionsService.Create(new ToursExcursions()
            {
                TourId = 3,
                ExcursionId = 2
            });

            _toursExcursionsService.Create(new ToursExcursions()
            {
                TourId = 3,
                ExcursionId = 3
            });
        }
    }
}