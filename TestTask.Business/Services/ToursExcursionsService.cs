﻿using System.Collections.Generic;
using TestTask.Business.Interfaces.Services;
using TestTask.Persistence.Interfaces.TableDataGateways;
using TestTask.Persistence.Models;

namespace TestTask.Business.Services
{
    public class ToursExcursionsService : IToursExcursionsService
    {
        private ITableDataGateway<ToursExcursions> _toursExcursionsGateway;

        public ToursExcursionsService(ITableDataGateway<ToursExcursions> toursExcursionsGateway)
        {
            _toursExcursionsGateway = toursExcursionsGateway;
        }

        public IEnumerable<ToursExcursions> GetAll()
        {
            return _toursExcursionsGateway.FindAll();
        }

        public ToursExcursions GetById(int id)
        {
            return _toursExcursionsGateway.FindById(id);
        }

        public void Create(ToursExcursions item)
        {
            _toursExcursionsGateway.Insert(item);
        }

        public void Update(int id, ToursExcursions item)
        {
            _toursExcursionsGateway.Update(id, item);
        }

        public void Delete(int id)
        {
            _toursExcursionsGateway.Delete(id);
        }
    }
}