﻿using System.Collections.Generic;
using TestTask.Business.Interfaces.Services;
using TestTask.Persistence.Interfaces.TableDataGateways;
using TestTask.Persistence.Models;

namespace TestTask.Business.Services
{
    public class AccommodationService : IAccommodationService
    {
        private ITableDataGateway<Accommodation> _accommodationGateway;

        public AccommodationService(ITableDataGateway<Accommodation> accommodationGateway)
        {
            _accommodationGateway = accommodationGateway;
        }

        public IEnumerable<Accommodation> GetAll()
        {
            return _accommodationGateway.FindAll();
        }

        public Accommodation GetById(int id)
        {
            return _accommodationGateway.FindById(id);
        }

        public void Create(Accommodation item)
        {
            _accommodationGateway.Insert(item);
        }

        public void Update(int id, Accommodation item)
        {
            _accommodationGateway.Update(id, item);
        }

        public void Delete(int id)
        {
            _accommodationGateway.Delete(id);
        }
    }
}