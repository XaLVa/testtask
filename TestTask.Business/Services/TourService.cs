﻿using System.Collections.Generic;
using TestTask.Business.Interfaces.Services;
using TestTask.Persistence.Interfaces.TableDataGateways;
using TestTask.Persistence.Models;

namespace TestTask.Business.Services
{
    public class TourService : ITourService
    {
        private ITableDataGateway<Tour> _tourGateway;

        public TourService(ITableDataGateway<Tour> tourGateway)
        {
            _tourGateway = tourGateway;
        }

        public IEnumerable<Tour> GetAll()
        {
            return _tourGateway.FindAll();
        }

        public Tour GetById(int id)
        {
            return _tourGateway.FindById(id);
        }

        public void Create(Tour item)
        {
            _tourGateway.Insert(item);
        }

        public void Update(int id, Tour item)
        {
            _tourGateway.Update(id, item);
        }

        public void Delete(int id)
        {
            _tourGateway.Delete(id);
        }
    }
}