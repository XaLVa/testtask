﻿using System.Collections.Generic;
using TestTask.Business.Interfaces.Services;
using TestTask.Persistence.Interfaces.TableDataGateways;
using TestTask.Persistence.Models;

namespace TestTask.Business.Services
{
    public class ExcursionService : IExcursionService
    {
        private ITableDataGateway<Excursion> _excursionGateway;

        public ExcursionService(ITableDataGateway<Excursion> excursionGateway)
        {
            _excursionGateway = excursionGateway;
        }

        public IEnumerable<Excursion> GetAll()
        {
            return _excursionGateway.FindAll();
        }

        public Excursion GetById(int id)
        {
            return _excursionGateway.FindById(id);
        }

        public void Create(Excursion item)
        {
            _excursionGateway.Insert(item);
        }

        public void Update(int id, Excursion item)
        {
            _excursionGateway.Update(id, item);
        }

        public void Delete(int id)
        {
            _excursionGateway.Delete(id);
        }
    }
}