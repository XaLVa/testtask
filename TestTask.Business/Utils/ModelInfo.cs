﻿using System.Text;
using TestTask.Persistence.Models;

namespace TestTask.Business.Utils
{
    public class ModelInfo
    {
        public static void AppendInfo(StringBuilder sb, string info, int colWidth)
        {
            if (sb == null || info == null) return;

            sb.Append(info);

            for (int i = 0, size = colWidth - info.Length; i < size; ++i)
            {
                sb.Append(" ");
            }
        }

        public static string GetTableTop(object obj, int colWidth)
        {
            if (obj == null) return "";

            var sb = new StringBuilder();

            var properties = obj.GetType().GetProperties();

            foreach (var property in properties)
            {
                AppendInfo(sb, property.Name, colWidth);
            }

            return sb.ToString();
        }

        public static string GetTableTop<T>(T obj, int colWidth)
        {
            if (!(obj is Entity)) return GetTableTop((object) obj, colWidth);

            var sb = new StringBuilder();

            AppendInfo(sb, "Id", colWidth);

            var properties = obj.GetType().GetProperties();

            foreach (var property in properties)
            {
                if (property.Name == "Id") continue;

                AppendInfo(sb, property.Name, colWidth);
            }

            return sb.ToString();
        }

        public static string GetTableRow(object obj, int colWidth)
        {
            if (obj == null) return "";

            var sb = new StringBuilder();

            var properties = obj.GetType().GetProperties();

            foreach (var property in properties)
            {
                AppendInfo(sb, property.GetValue(obj)?.ToString(), colWidth);
            }

            return sb.ToString();
        }

        public static string GetTableRow(Entity obj, int colWidth)
        {
            if (obj == null) return "";

            var sb = new StringBuilder();

            AppendInfo(sb, obj.Id.ToString(), colWidth);

            var properties = obj.GetType().GetProperties();

            foreach (var property in properties)
            {
                if (property.Name == "Id") continue;

                AppendInfo(sb, property.GetValue(obj)?.ToString(), colWidth);
            }

            return sb.ToString();
        }
    }
}