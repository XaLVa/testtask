﻿using System.Collections.Generic;
using System.Linq;
using TestTask.Business.Interfaces.Services;
using TestTask.Business.Utils;
using TestTask.Persistence.Models;
using TestTask.Persistence.Utils;
using static System.Console;

namespace TestTask.Presentation
{
    public class App
    {
        private const int ColWidth = 20;
        private const string DelimiterString = "--------------------------------------------------------------------------------";

        private ITestTaskService _testTaskService;
        private ITourService _tourService;
        private IFlightService _flightService;
        private IAccommodationService _accommodationService;
        private IExcursionService _excursionService;
        private IToursExcursionsService _toursExcursionsService;

        public App(ITestTaskService testTaskService, ITourService tourService, IFlightService flightService, IAccommodationService accommodationService, IExcursionService excursionService, IToursExcursionsService toursExcursionsService)
        {
            _testTaskService = testTaskService;
            _tourService = tourService;
            _flightService = flightService;
            _accommodationService = accommodationService;
            _excursionService = excursionService;
            _toursExcursionsService = toursExcursionsService;
        }

        public void Start()
        {
            _testTaskService.Init();
            WriteAllTables();
            WriteTable(_testTaskService.GetToursWithExcursions(), "Tours with excursions");
            WriteTable(_testTaskService.GetTourMaxExcursionPrices(), "Tours with excursion max prices");
            WriteTable(_testTaskService.GetTourTotalPrices(), "Tours total prices");
        }

        private void WriteAllTables()
        {
            WriteTable(_tourService.GetAll(), "Tours");
            WriteTable(_flightService.GetAll(), "Flights");
            WriteTable(_accommodationService.GetAll(), "Accommodations");
            WriteTable(_excursionService.GetAll(), "Excursions");
            WriteTable(_toursExcursionsService.GetAll(), "ToursExcursions");
        }

        private void WriteTable<T>(IEnumerable<T> items, string tableTitle = "", int colWidth = ColWidth)
        {
            if (items == null) return;

            WriteLine(DelimiterString);

            WriteLine(tableTitle);

            T first = items.FirstOrDefault();

            if (first == null) return;

            WriteLine(ModelInfo.GetTableTop(first,colWidth));

            foreach (var item in items)
            {
                WriteLine(ModelInfo.GetTableRow(item, colWidth));
            }

            WriteLine(DelimiterString);
        }
    }
}