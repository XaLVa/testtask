﻿using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TestTask.Business.Interfaces.Services;
using TestTask.Business.Services;
using TestTask.Persistence;
using TestTask.Persistence.Interfaces;
using TestTask.Persistence.Interfaces.Sqlite;
using TestTask.Persistence.Interfaces.TableDataGateways;
using TestTask.Persistence.Models;
using TestTask.Persistence.Sqlite;
using TestTask.Persistence.TableDataGateways;

namespace TestTask.Presentation
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);
            var service = services.BuildServiceProvider();
            service.GetService<App>().Start();
        }

        static void ConfigureServices(IServiceCollection services)
        {
            var configuration = 
                new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();

            services.AddSingleton(configuration);

            services.AddSingleton<IConnectionProvider>(
                new ConnectionProvider(configuration.GetConnectionString("Default")));

            services.AddSingleton<ITableDataGateway<Tour>, TourTableDataGateway>();
            services.AddSingleton<ITableDataGateway<Flight>, FlightTableDataGateway>();
            services.AddSingleton<ITableDataGateway<Accommodation>, AccommodationTableDataGateway>();
            services.AddSingleton<ITableDataGateway<Excursion>, ExcursionTableDataGateway>();
            services.AddSingleton<ITableDataGateway<ToursExcursions>, ToursExcursionsTableDataGateway>();

            services.AddSingleton<IInitializer, TestTaskInitializer>();
            services.AddSingleton<ITestTaskQueryExecutor, TestTaskQueryExecutor>();

            services.AddTransient<ITestTaskService, TestTaskService>();
            services.AddTransient<ITourService, TourService>();
            services.AddTransient<IFlightService, FlightService>();
            services.AddTransient<IAccommodationService, AccommodationService>();
            services.AddTransient<IExcursionService, ExcursionService>();
            services.AddTransient<IToursExcursionsService, ToursExcursionsService>();

            services.AddSingleton<App, App>();
        }
    }
}
