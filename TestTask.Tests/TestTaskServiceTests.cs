﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using TestTask.Business.Interfaces.Services;
using TestTask.Business.Services;
using TestTask.Persistence;
using TestTask.Persistence.Interfaces;
using TestTask.Persistence.Interfaces.Sqlite;
using Xunit;

namespace TestTask.Tests
{
    public class TestTaskServiceTests
    {
        private ITestTaskService _testTaskService;

        public TestTaskServiceTests()
        {
            var taskQueryExecutorMock = new Mock<ITestTaskQueryExecutor>();
            taskQueryExecutorMock.Setup(x => x.GetToursWithExcursions()).Returns(GetToursWithExcursions());
            taskQueryExecutorMock.Setup(x => x.GetTourMaxExcursionPrices()).Returns(GetTourMaxExcursionPrices());
            taskQueryExecutorMock.Setup(x => x.GetTourTotalPrices()).Returns(GetTourTotalPrices());
            _testTaskService = new TestTaskService(new Mock<IInitializer>().Object, new Mock<ITourService>().Object, new Mock<IFlightService>().Object, new Mock<IAccommodationService>().Object, new Mock<IExcursionService>().Object, new Mock<IToursExcursionsService>().Object, taskQueryExecutorMock.Object);
        }

        [Fact]
        public void GetToursWithExcursions_Returns_Success()
        {
            // Arrange
            var testTaskService = _testTaskService;

            // Act
            var result = _testTaskService.GetToursWithExcursions();

            // Assert
            Assert.True(result.SequenceEqual(GetToursWithExcursions()));
        }

        [Fact]
        public void GetTourMaxExcursionPrices_Returns_Success()
        {
            // Arrange
            var testTaskService = _testTaskService;

            // Act
            var result = _testTaskService.GetTourMaxExcursionPrices();

            // Assert
            Assert.True(result.SequenceEqual(GetTourMaxExcursionPrices()));
        }

        [Fact]
        public void GetTourTotalPrices_Returns_Success()
        {
            // Arrange
            var testTaskService = _testTaskService;

            // Act
            var result = _testTaskService.GetTourTotalPrices();

            // Assert
            Assert.True(result.SequenceEqual(GetTourTotalPrices()));
        }

        private IEnumerable<object> GetToursWithExcursions()
        {
            return new List<object>()
            {
                new
                {
                    TourId = 1,
                    ExcursionTitle = "A"
                },

                new
                {
                    TourId = 2,
                    ExcursionTitle = "B"
                }
            };
        }

        private IEnumerable<object> GetTourMaxExcursionPrices()
        {
            return new List<object>()
            {
                new
                {
                    TourId = 1,
                    MaxPrice = 20
                },

                new
                {
                    TourId = 2,
                    MaxPrice = 30
                }
            };
        }

        private IEnumerable<object> GetTourTotalPrices()
        {
            return new List<object>()
            {
                new
                {
                    TourId = 1,
                    TotalPrice = 100
                },

                new
                {
                    TourId = 2,
                    TotalPrice = 90
                }
            };
        }
    }
}